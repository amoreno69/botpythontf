import os, sys
import re
from os import listdir
from os.path import isfile, join
import tkinter as tk
from PIL import Image, ImageTk
from imagesearch import *
import pyscreenshot as ImageGrab
img = None
root = None
indexes = ["5","Cruz","1","7","2","4","6","3"]
def save(index, image):
    mypath = str("clasificadas/" + index)
    files = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    file_id = files.__len__()
    filename = mypath + "/" + str(file_id + 1) + ".png"
    image.save(filename)
def key(event): 
    global img
    global root
    indexes.append(str(event.char))
    root.destroy()
if __name__ == '__main__':
    files = [f for f in listdir("unclassified") if isfile(join("unclassified", f))]
    ordered_files = sorted(files, key=lambda x: (int(re.sub('\D', '', x)), x))
    for i, file in enumerate(ordered_files):
        if (i == 8):
            break 
        root = tk.Tk()
        image = Image.open("unclassified/" + file)
        my_image = image.resize((360, 250), Image.ANTIALIAS) 
        filename = ImageTk.PhotoImage(my_image) 
        panel = tk.PanedWindow()
        panel = tk.Label(root, image=filename)
        root.bind('<F1>', key)
        panel.pack(side="bottom", fill="both", expand="yes")
        root.focus_force()
        root.mainloop()
    i = 0
    for file in ordered_files :
        image = Image.open("unclassified/" + file)
        save(indexes[i], image)
        i+=1
        if (i == 8):
            i = 0
    print("done")



