import argparse
import sys
import cv2
import random
import time
import os
import pyautogui
from imagesearch import *
from os import listdir
from os.path import isfile, join
import numpy as np
import tensorflow as tf

def preprocess(im, index):
    im = np.array(im)
    im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY).flatten()
    N[index] = im

pos = imagesearch("character.PNG")
pos1 = imagesearch("kernel.PNG")

def get_images():
    pos = imagesearch("character.PNG")
    pos1 = imagesearch("kernel.PNG")
    pos1 = list(pos1)
    pos1[1] -= 5
    print(pos1)
    print("capturing the images")
    scene = pyautogui.screenshot()
    character2 = scene.crop((pos[0] - 170, pos[1] - 18, pos[0] - 115, pos[1] + 25))
    character1 = scene.crop((pos[0] - 100, pos[1] - 53, pos[0] - 45, pos[1] - 10))
    character3 = scene.crop((pos[0] - 100, pos[1] + 17, pos[0] - 45, pos[1] + 60))
    character4 = scene.crop((pos[0] + -30, pos[1] + 52, pos[0] + 25, pos[1] + 95))
    character6 = scene.crop((pos[0] + 110, pos[1] - 18, pos[0] + 165, pos[1] + 25))
    character5 = scene.crop((pos[0] + 40, pos[1] + 17, pos[0] + 95, pos[1] + 60))
    character7 = scene.crop((pos[0] + 40, pos[1] - 55, pos[0] + 95, pos[1] - 12))
    cat1 = scene.crop((pos1[0] -100, pos1[1] - 57, pos1[0] - 45, pos1[1] - 14))
    cat2 = scene.crop((pos1[0] - 172, pos1[1] - 21, pos1[0] - 117, pos1[1] + 22))
    cat3 = scene.crop((pos1[0] - 105, pos1[1] + 14, pos1[0] - 50, pos1[1] + 57))
    cat4 = scene.crop((pos1[0] + -32, pos1[1] + 49, pos1[0] + 23, pos1[1] + 92))
    cat5 = scene.crop((pos1[0] + 35, pos1[1] + 17, pos1[0] + 90, pos1[1] + 60))
    cat6 = scene.crop((pos1[0] + 110, pos1[1] - 21, pos1[0] + 165, pos1[1] + 22))
    cat7 = scene.crop((pos1[0] + 40, pos1[1] - 56, pos1[0] + 95, pos1[1] - 13))
    cat8 = scene.crop((pos1[0] + -32, pos1[1] - 93, pos1[0] + 23, pos1[1] - 50))
    preprocess(character1, 0)
    preprocess(character2, 1)
    preprocess(character3, 2)
    preprocess(character4, 3)
    preprocess(character5, 4)
    preprocess(character6, 5)
    preprocess(character7, 6)
    preprocess(cat1, 7)
    preprocess(cat2, 8)
    preprocess(cat3, 9)
    preprocess(cat4, 10)
    preprocess(cat5, 11)
    preprocess(cat6, 12)
    preprocess(cat7, 13)
    preprocess(cat8, 14)
    return pos

lista_pred = []

def solve(N, ind):
    X = np.zeros((1, 2365), np.int32)
    X[0] = N[ind]

    prediction = 0

    with tf.Session() as session:
        saver = tf.train.import_meta_graph("mod.meta")
        saver.restore(session, tf.train.latest_checkpoint(""))
        graph = tf.get_default_graph()
        x = tf.placeholder(tf.float32, [None, 2365])
        W = graph.get_tensor_by_name("weights:0")
        b = graph.get_tensor_by_name("biases:0")
        y = tf.argmax(tf.nn.softmax(tf.matmul(x, W) + b), axis=1)

        P = session.run(y, feed_dict={x: X})
        prediction = P[0] + 1

    return prediction


while True:
    lista_pred = []
    pos = imagesearch("Col.PNG")
    if pos[0] != -1:
        pyautogui.click(button="right")
        print("position : ", pos[0], pos[1])
        pyautogui.moveTo(pos[0]+10, pos[1]+10)
        time.sleep(0.1)
        pyautogui.click(button="right")
        time.sleep(0.1)
        pos2 = imagesearch("Mano.PNG")
        if pos2[0] != -1:
            pyautogui.moveTo(pos2[0], pos2[1])
            time.sleep(0.1)
            pyautogui.click(button="left")
            time.sleep(4)
        pos = imagesearch("Col_baja.PNG")
        if pos[0] != -1:
            print("position : ", pos[0], pos[1])
            pyautogui.moveTo(pos[0]+10, pos[1]+10)
            time.sleep(0.1)
            pyautogui.click(button="right")
            time.sleep(0.1)
            pos2 = imagesearch("tijeras.PNG")
            if pos2[0] != -1:
                pyautogui.moveTo(pos2[0], pos2[1])
                time.sleep(0.1)
                pyautogui.click(button="left")
                time.sleep(4)
    else:
        pos = imagesearch("Tierra.PNG",0.38)
        if pos[0] != -1:
            ic = imagesearch("Icono.PNG")
            pyautogui.moveTo(ic[0]+10, ic[1]+10)
            time.sleep(0.1)
            pyautogui.click(button="left")
            time.sleep(0.1)
            print("position : ", pos[0], pos[1])
            pyautogui.moveTo(pos[0]+20, pos[1]+20)
            time.sleep(0.1)
            pyautogui.click(button="left")
            time.sleep(4)
        pos2 = imagesearch("Tierra2.PNG",0.38)
        if pos2[0] != -1:
            ic = imagesearch("Icono_col.PNG")
            pyautogui.moveTo(ic[0]+10, ic[1]+10)
            time.sleep(0.1)
            pyautogui.click(button="left")
            time.sleep(0.1)
            print("position : ", pos[0], pos[1])
            pyautogui.moveTo(pos2[0]+20, pos2[1]+20)
            time.sleep(0.1)
            pyautogui.click(button="left")
            time.sleep(4)
        else:
            print("doramio")
    arma = imagesearch("espada.PNG")
    restart = imagesearch("restart.PNG")
    if arma[0] != -1 or restart[0] != -1:
        pos = imagesearch("character.PNG")
        pos1 = imagesearch("kernel.PNG")
        if arma[0] != -1:
            pyautogui.moveTo(arma[0], arma[1])
            time.sleep(0.1)
            pyautogui.click(button="left")
        if restart[0] != -1:
            pyautogui.moveTo(10, 10)
        time.sleep(2)
        N = np.zeros((15, 2365), np.int32)
        get_images()
        for i in range(15):
            lista_pred.append(solve(N, i))
        chr = lista_pred[:7]
        ct = lista_pred[7:]
        print(chr)
        print(ct)
        indices = []
        for i in ct:
            if i != 9:
                indices.append(i)
        positions = []
        for indice in indices:
            try:
                position_of_the_square = chr.index(indice)
                positions.append(position_of_the_square)
            except ValueError:
                positions.append(7)
        print(positions)
        for posi in positions:
            time.sleep(0.5)
            pos3 = imagesearch("atac.PNG")
            if pos3[0] != -1:
                pyautogui.moveTo(pos3[0],pos3[1])
                pyautogui.click(button="left")
            if posi == 0:
                pyautogui.moveTo(pos[0] - 100 + 20, pos[1] - 53 + 20)
                pyautogui.click(button="left")
            if posi == 1:
                pyautogui.moveTo(pos[0] - 170 + 20, pos[1] - 18 + 20)
                pyautogui.click(button="left")
            if posi == 2:
                pyautogui.moveTo(pos[0] - 105 + 20, pos[1] + 17 + 20)
                pyautogui.click(button="left")
            if posi == 3:
                pyautogui.moveTo(pos[0] + -30 + 20, pos[1] + 52 + 20)
                pyautogui.click(button="left")
            if posi == 4:
                pyautogui.moveTo(pos[0] + 40 + 20, pos[1] + 17 + 20)
                pyautogui.click(button="left")
            if posi == 5:
                pyautogui.moveTo(pos[0] + 110 + 20, pos[1] - 18 + 20)
                pyautogui.click(button="left")
            if posi == 6:
                pyautogui.moveTo(pos[0] + 40 + 15, pos[1] - 55 + 15)
                pyautogui.click(button="left")
            if posi == 7:
                pyautogui.moveTo(pos[0] + -32 + 20, pos[1] - 93 + 20)
                pyautogui.click(button="left")
        time.sleep(4)
    cruz = imagesearch("cruz.PNG")
    if cruz[0] != -1:
        pyautogui.moveTo(cruz[0],cruz[1])
        time.sleep(0.1)
        pyautogui.click(button="left")
        